#!/bin/bash

sudo docker build -t vfarcic/logstash .

sudo docker run -d --name logstash \
  -v /data/logstash/logstash.conf:/ls/logstash.conf \
  -v /data/logstash/patterns:/ls/patterns \
  -v /data/services-normalizator/logs:/data/services-normalizator/logs \
  -v /data/services-demo/logs:/data/services-demo/logs \
  -v /data/event-bus/innova/logs:/data/event-bus/innova/logs \
  -v /data/event-bus/mutua/logs:/data/event-bus/mutua/logs \
  -v /opt/sisnet/mdp/Logs:/data/innova/logs \
  vfarcic/logstash
sudo docker logs -f logstash
sudo docker rm -f logstash

sudo docker run -it --rm --name logstash \
  -v /data/logstash/logstash.conf:/ls/logstash.conf \
  -v /data/services-normalizator/logs:/data/services-normalizator/logs \
  -v /data/services-demo/logs:/data/services-demo/logs \
  -v /data/services-catastro/logs:/data/services-catastro/logs \
  -v /data/event-bus/innova/logs:/data/event-bus/innova/logs \
  -v /data/event-bus/mutua/logs:/data/event-bus/mutua/logs \
  -v /data/innova/logs:/data/innova/logs \
  -v /data/logstash/patterns:/ls/patterns \
  vfarcic/logstash /bin/bash

sudo docker run -it --rm --name logstash \
  -v /home/vifarcic/IdeaProjects/mdp-servers/ansible/roles/logstash/templates/logstash.conf:/ls/logstash.conf \
  -v /home/vifarcic/IdeaProjects/mdp-servers/ansible/roles/logstash/files/patterns:/ls/patterns \
  -v /home/vifarcic/IdeaProjects/mdp-servers/ansible/roles/logstash/files:/data/logs \
  vfarcic/logstash /bin/bash

curl -XDELETE 'http://localhost:9200/logstash-2011.05.19?pretty'