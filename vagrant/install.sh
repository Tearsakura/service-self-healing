#!/bin/sh

# Add Repositories
sudo apt-get install -y software-properties-common
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-add-repository ppa:ansible/ansible
sudo sh -c "echo deb https://get.docker.io/ubuntu docker main \
> /etc/apt/sources.list.d/docker.list"
sudo apt-get update

# Install Java8
sudo echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
    sudo apt-get -y install --no-install-recommends oracle-java8-installer
sudo apt-get -y install oracle-java8-set-default

# Install Ansible
sudo apt-get install -y ansible